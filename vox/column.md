## Authoritarian Elites

### Intro:Abstract
Compared with the wealth of scholarly work on the finer institutional effects of democratic systems, comparatively little is known about the inner workings of autocratic regimes. This imbalance, due partly to the paucity of available data on these regimes, has dire consequences for policymaking, statecraft, and development research. However, if researchers are willing to consider the nontraditional sources of data suggested by the theory of authoritarianism, key insights can be recovered. In particular, we discuss the evidence these data have brought to bear against the winner-take-all hypothesis of authoritarian governance, and remarkable 'inclusivity' of autocratic polities, with examples from Sub Saharan Africa, China, and North Korea.

### Intro:Body
On the basis of welfare effects, one hardly need justify the study of autocracy. The calculus of autocratic governance all too often results in the phenomena of intra and inter-state violent conflict, expropriation, factor misallocation, and the misappropriation of international aid and humanitarian efforts. As a step in understanding these outcomes, we must understand this internal calculus. Here we arrive at two fundamental problems. The first is that the data used to study democratic regimes are often less relevant in authoritarian contexts. The second is that access to quality data is often difficult as these regimes are far more inclined to secrecy than open-access states. 

We discuss a new strain in the political economy of development literature that seeks to overcome these problems by employing novel sources of data to uncover the inner workings of authoritarian regimes. This literature takes as its starting point the theory of autocracy developed by North et al. (2009), who argue that personal connections and identity are particularly key variables in the authoritarian states. {"Personal relationships, who one is and who one knows, form the basis for social organization and constitute the arena for individual interaction, particularly personal relationships among powerful individuals"}. This literature takes seriously the claim by Mesquita and Smith (2012) that "no leader, no matter how august or revered, ever rules alone." It therefore seeks to collect data on the key individuals that comprise these regimes - the authoritarian elites.  This literature, though small, has already yielded new insights. One key finding is that the internal structure of authoritarian regimes exhibits a remarkable degree of balanced inclusiveness. This finding is replicated across three contexts, and rejects the winner-take-all logic that often pervades the discourse and scholarship on authoritarian regimes. 

// Furthermore, this has important implications for policy that is invested in the leader at the expense of regime elites.

// Such an insight is crucial for policy, as it both suggests that the current focus on the identity and preferences of the leader of these regimes is misplaced, and that a lack of inclusiveness is not a pervasive problem of authoritarian governance

### Africa
The post-colonial dictators of Sub-Saharan Africa are often characterized by this winner-take-all approach to authoritarian polities. The Big Man theory of autocracy models the autocrat as a unitary actor, the winner of a contest where the prize is unchecked power over the state apparatus and its constituents. In contrast, Francois et al (2015) empirically demonstrate the importance of ethnic inclusiveness for building stable ruling coalitions. The authors focus on executive branch ministerial posts, and collect data on the ethnic identity of cabinet members between 1960-2004 in fifteen SSA countries. As Figure X shows, they find that the cabinet seat allocations across ethnic groups closely mirror the population shares of these groups within the country. Indeed, compared to a selection of democratic polities, these SSA regimes appear both more inclusive and more proportional.

### China
Similarly, Francois et al (2016) identify a remarkable pattern of factional balance within the Chinese Communist Party. They collect biographical information on CCP political elites at or above the provincial level, and assign elites to either the Military, Princeling, Hu Jintao, or Jiang Zemin factions based on the collected data. Matching these factional assignments with data collected on positions in the CCP leadership, they find that government role assignments to the two main rival political factions are horizontally balanced across all levels of government. They use their model to generate predictions (in advance) of the likely factional alignment of the Politbureau standing committee (China's highest executive body) following the 19th Party Congress. These predictions, contrary to much of the recieved wisdom at the time, proved remarkably accurate ex post.

### Korea
Finally, we discuss some recent work on North Korea as a final proof of concept. North Korea is a particularly opaque environment to study, even by the foregoing standards of autocratic regimes. Nonetheless, insights are possible from the limited data available. Newson and Trebbi (2018) collect press releases from the state propaganda ministry that list the identity of attendees of public state events (factory visits, missile tests, etc). From these data they create a database of regime notables and arrange them in two elite networks, with edges between elites representing coappearance at regime events. The first network covers the turbulent transitionary period between the Kim Jong Il and Kim Jong Un regimes from 2009 to 2012. The second covers the first four years of Kim Jong Un's rule, from 2013 to 2016. They interpret event appearance as a noisy signal of elite importance, and among their findings draw draw two relevant to the present article:

   + During the Kim Jong Un regime, when elites are assigned to party or military factions according to their announced roles by the state propaganda ministry, the internal regime network is strikingly balanced between the two factions
   + The authors factional assignment is validated by an unsupervised learning algorithm that uncovers two main communities with similar membership
   
The authors also find that this internal balance is less present during the transitionary period, where factional assignment agrees less with the latent structure uncovered by the community detection algorithm. The roughly equal representation of these factions in the regime is a priori surprising and appears to contradict the view of North Korea as a unitary actor or unchallenged military dictatorship.

### Conclusion
There are several broader conclusions to be drawn from this pattern of internal balance. In each article the authors interpret this inclusivity within a particular institutional context: ethnic patronage to assuage coup threats in SSA, the tournament style promotion structure within the CCP, and factional jostling for visibility at North Korea state events. One outcome these mechanisms share is a surprising inclusivity that challenges the recieved wisdom of authoritarian politics. Fundamentally, it may be that the possibility of violence intrinsic to authoritarian rule may, somewhat counterintuitively, lead to more inclusiveness. 


### Works Cited

De Mesquita, B. B., & Smith, A. (2012). The Dictator’s Handbook: Why Bad Behavior is Almost Always Good Politics. Public Affairs. 

Francois, P., Rainer, I., & Trebbi, F. (2015). How Is Power Shared in Africa? Econometrica, 83(2), 465–503. 

Francois, P., Trebbi, F., & Xiao, K. (2016). Factions in Nondemocracies : Theory and Evidence from the Chinese Communist Party. NBER Working Paper series.

Newson, A., & Trebbi, F. (2018). Authoritarian Elites. NBER Working Paper Series.

North, D. C., Wallis, J. J., & Weingast, B. R. (2009). Violence and Social Orders. Cambridge University Press. 
