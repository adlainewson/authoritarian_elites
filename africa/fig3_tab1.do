clear all
set more off

* Globals
global countryname ""Luxembourg" "Netherlands" "Norway" "New Zealand" "Denmark" "Italy" "Spain" "Canada" "Australia" "Sweden" "Ireland" "Switzerland" "Greece" "France" "United Kingdom" "Germany" "Iceland" "Austria" "Portugal" "Belgium" "Finland" "Japan""
global country2 ""LUX" "NLD" "NOR" "NZL" "DNK" "ITA" "ESP" "CAN" "AUS" "SWE" "IRL" "CHE" "GRC" "FRA" "GBR" "DEU" "ISL" "AUT" "PRT" "BEL" "FIN" "JPN""
global country ""AUS" "AUT" "BEL" "DNK" "FIN" "DEU" "ISL" "IRL" "ITA" "LUX" "NLD" "NOR" "PRT" "SWE"

* Vote shares
use "seat&vote_PARLGOV.dta", clear
rename country countrycode
rename country_name_short country
* only national elections
drop if election_type=="ep"
gen tokeep=.
foreach x of global country {
	replace tokeep=1 if country=="`x'"
}
keep if tokeep==1
gen voteshare=vote_share 
gen seatshare=  100*seats/seats_total
drop if election_id==.
codebook party_id election_id
* Date
format  election_date %td
gen year=year(election_date)
keep if year>=1960&year<2005
sort election_id party_id
save coalition.dta, replace

* Government coalitions
use "coalition_PARLGOV.dta", clear
rename country countrycode
rename country_name_short country
gen tokeep=.
foreach x of global country {
	replace tokeep=1 if country=="`x'"
}
keep if tokeep==1
drop if election_id==.
rename cabinet_id gov_id
rename  cabinet_party coalitionmember
* Mark observations without a ruling coalition (bad obs if check==0)
bysort country gov_id: egen check = total(coalitionmember)
* Date
format  election_date %td
format  start_date %td
gen year=year(election_date)
gen year_gov=year(start_date)
gen month_gov=month(start_date)
keep if year>=1960&year<2005

* Merge government and vote share data
codebook party_id election_id
sort  election_id party_id
merge election_id party_id using coalition.dta
tab _m

* Forget about parties that do not get any representation in parliament
keep if _m==3
drop _m

* In order to check whether dropped parties due to changes in id in the data can bias representation measures
bysort country gov_id: egen tvoteshare=total(voteshare) if gov_id~=.
bysort country gov_id: egen tseatshare=total(seatshare) if gov_id~=.
* seems ok but let me drop extrema
sum tvoteshare tseatshare, det
replace voteshare=. if (tvoteshare>110|tvoteshare<90)&tvoteshare~=.
replace voteshare=. if check==0
replace coalitionm=. if check==0

* Shares of votes unrepresented in the government coalition
gen tmp = coalitionm*voteshare
bysort country gov_id: egen cabvoteshare=total(tmp) if voteshare~=.
gen unrepresented = 100-cabvoteshare
lab variable unrepres "Share of Voters Not Represented in Government"

* Keep 1 obs. per country-cabinet
bysort country gov_id: keep if _n==1
keep country year_gov month_gov unrepresented
* Fill in the sample
fillin country year_gov month_gov
bysort country (year month): replace unrepresented=unrepresented[_n-1] if unrepresented==.
bysort year month: egen oecd_unrepres = mean(unrepresented)

sort year month
gen date=ym(year, month)
format date %tm
lab variable country "Country"
lab variable date "Year"
lab variable oecd_unrepres "Share of Voters Not Represented in Government"
gen countryname ="" 
replace countryname ="Australia" if country=="AUS"
replace countryname ="Austria" if country=="AUT"
replace countryname ="Belgium" if country=="BEL"
replace countryname ="Finland" if country=="FIN"
replace countryname ="Denmark" if country=="DNK"
replace countryname ="Germany" if country=="DEU"
replace countryname ="Iceland" if country=="ISL"
replace countryname ="Ireland" if country=="IRL"
replace countryname ="Italy" if country=="ITA"
replace countryname ="Luxembourg" if country=="LUX"
replace countryname ="Netherlands" if country=="NLD"
replace countryname ="Norway" if country=="NOR"
replace countryname ="Portugal" if country=="PRT"
replace countryname ="Sweden" if country=="SWE"
rename countryname Country

gen tmp = dofm(date)
gen yr = year(tmp)
drop tmp
twoway (line unrepres yr, cmiss(n) ), ytitle(,size(small)) by(Country) xla(1960 1980 2004) note(" ") 
graph export excluded_democ.eps,replace
*note("Sample: Ansolabehere et al. 2005." "Based on Vote Shares from D�ring and Manow, 2010")
drop country
rename Country country
global country ""Australia" "Austria" "Belgium" "Denmark" "Finland" "Germany" "Iceland" "Ireland" "Italy" "Luxembourg" "Netherlands" "Norway" "Portugal" "Sweden"


collapse (mean) unrepres (sd) unrepres_sd=unrepres,by(country)
