*** Fit graph


clear all
set more off

***********************************************************************************
*Figure 1
***********************************************************************************

use "datasets/tmp3_orig.dta", clear
sort govshare
drop if leadergroup==1
drop if popshare>.4
#delim ;
twoway 
   scatter govshare popshare if popshare<.4, msize(tiny) msy(Oh) mcolor(black*.8)  yti("Ethnic Group's Cabinet Share") xti("Ethnic Group's Population Share") 
   ||
   lowess govshare popshare if popshare<.4, bw(.8) lpa(longdash)  lcolor(blue*.8)
   ||
   lfit govshare popshare if popshare<.4,  lpa(dash_dot) lcolor(orange*.8)
   ||
   line govshare govshare if govshare<.4, lpa(dash) lcolor(red*.8)

   xscale(noextend)
   xlabel(0(.1).4)
   //aspectratio(.9)
   xsize(5)
   legend(
    col(2) pos(6)
    order(
       1 "Share of Posts to Non-leader Groups" 
       2 "Nonparametric Fit (Lowess)" 
       3 "Linear Fit" 
       4 "45 Degree Line"
       ) 
    )
   ;
    #delim cr
    graph export final/fit.eps,replace
