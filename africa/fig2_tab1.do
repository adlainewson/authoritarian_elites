** Excluded Africa graphs + excluded africa table

clear all
set more off

****
* Load data
use "datasets/ethnicgroup_data.dta", clear
 
* Globals
global country  "Benin Cameroon "Cote d'Ivoire" "Congo Kinshasa" Gabon Ghana Guinea Kenya Liberia Nigeria "Republic of Congo" "Sierra Leone" Tanzania Togo Uganda"
global    variables "lsq       LH    rae    effno leadergovshare govsize"
global topvariables "toplsq topLH toprae topeffno leadertopgovshare"
global lowvariables "lowlsq lowLH lowrae loweffno leaderlowgovshare"

* Rankings in terms of positions in the cabinet
bysort country year (govpositions): gen rank = _n 

* Specific groups characteristics
gen leadergovsize   =leadergroup*govpositions
gen leadertopgovsize   =leadergroup*topgovpositions
gen leadergovshare   =leadergroup*abs(govshare-popshare)
gen leadertopgovshare=leadergroup*abs(topgovshare-popshare)
gen leaderlowgovshare=leadergroup*abs(lowgovshare-popshare)
gen largestgovshare=largestgroup*abs(govshare-popshare)
gen largesttopgovshare=largestgroup*abs(topgovshare-popshare)
gen unrepres=popshare*norepresentation

* Seat-value score
gen wgroup = 1
gen w=1
gen gscore = 100*wgroup*(topgovpositions*w +lowgovpositions)/(topgovsize*w+lowgovsize)
sum govshare gscore
replace govshare=gscore

* Generate elements for standard fractionalization measures
gen frac = (popshare/100)^2
gen gfrac= (govshare/100)^2
gen topgfrac= (topgovshare/100)^2
gen lowgfrac= (lowgovshare/100)^2

*** Generate elements for disproportionality measures (Gallagher, 1991)
gen LH     = abs(govshare-popshare)
gen lsq    = (govshare-popshare)^(2)
gen rae    = abs(govshare-popshare) if ethnicgroup~="Other"&popshare>=0.5
gen saint  = (1/popshare)*(govshare-popshare)^(2)
gen dhondt = govshare/popshare
gen dhondt5= govshare/popshare if popshare>=5
* Top positions only
gen topLH     = abs(topgovshare-popshare)
gen toplsq    = (topgovshare-popshare)^(2)
gen toprae    = abs(topgovshare-popshare) if ethnicgroup~="Other"&popshare>=0.5
gen topsaint  = (1/popshare)*(topgovshare-popshare)^(2)
gen topdhondt = topgovshare/popshare
gen topdhondt5= topgovshare/popshare if popshare>=5
* Low positions only
gen lowLH     = abs(lowgovshare-popshare)
gen lowlsq    = (lowgovshare-popshare)^(2)
gen lowrae    = abs(govshare-popshare) if ethnicgroup~="Other"&popshare>=0.5
gen lowsaint  = (1/popshare)*(lowgovshare-popshare)^(2)
gen lowdhondt = lowgovshare/popshare
gen lowdhondt5= lowgovshare/popshare if popshare>=5

* Other
gen diff = govshare-popshare
bysort country ethnicg (year_gov): gen diff_change=diff-diff[_n-1]
* Max gain
bysort country year: egen maxdiff = max(diff_change)
gen gainersize=popshare if diff_change==maxdiff
* Max loss
bysort country year: egen mindiff = min(diff_change) 
gen losersize=popshare if diff_change==mindiff

*** Country-year collapse 
collapse (sum) unrepres *gfrac frac *LH *lsq *saint /*
*/       (mean) *rae lsqmod=lsq /*
*/       (max) leadergovshare leadertopgovshare leaderlowgovshare *dhondt* *largestgovshare largesttopgovshare *transition *stayshare leadertopgovsize leadergovsize topgovsize govsize gainersize /*
*/       (min) losersize, by(country year_gov month_gov)
rename year_ year
lab var year "Year"

*** Complete disproportionality and other measures
replace frac = 1 - frac
* All
replace lsq = (1/2*lsq)^(1/2)
replace lsqmod=lsqmod^(1/2)
replace LH=1/2*LH
gen effno=1/gfrac
* Top
replace toplsq = (1/2*toplsq)^(1/2)
replace topLH=1/2*topLH
gen topeffno=1/topgfrac
* Low
replace lowlsq = (1/2*lowlsq)^(1/2)
replace lowLH=1/2*lowLH
gen loweffno=1/lowgfrac
* Collapse (sum) does place a 0 when all obs are missing for a country year obs and this is wrong
foreach var of global variables {
	replace `var'=. if govsize==.
} 
foreach var of global topvariables {
	replace `var'=. if topgovsize==.
}
foreach var of global lowvariables {
	replace `var'=. if govsize==.
}
* Generate leader and cabinet durations
gen leaderduration = 1
replace leaderduration =0 if leadertransition==1
bysort country (year): replace leaderduration = leaderduration[_n-1] + 1 if _n>1&leadertransition~=1
gen cabduration = 1
replace cabduration =0 if cabtransition==1
bysort country (year): replace cabduration = cabduration[_n-1] + 1 if _n>1&cabtransition~=1
gen leaderduration2 = leaderduration^2
* Generate lags and changes
qui foreach var of varlist govsize unrepres *stayshare *transition *lsq* *LH *rae *saint *dhondt *dhondt5 leadergovshare leadertopgovshare leaderlowgovshare largestgovshare largesttopgovshare *effno {
	bysort country (year): gen `var'_l=`var'[_n-1]
	bysort country (year): gen `var'_l2=`var'[_n-2]
	bysort country (year): gen `var'_l3=`var'[_n-3]
	gen `var'_change = `var'-`var'_l
	gen `var'_rchange = `var'/`var'_l - 1
}

qui foreach var of varlist leadertransition {
	gen pre`var'= `var'
	*bysort country (year):  replace pre`var'= 1 if `var'[_n+3]==1
	*bysort country (year):  replace pre`var'= 1 if `var'[_n+2]==1
	bysort country (year):  replace pre`var'= 1 if `var'[_n+1]==1
	gen post`var'= `var'+`var'_l
	*+`var'_l2+`var'_l3
}

lab var lsq "LSq"
lab var lsq_change "Change in LSq between t and t+1"
lab var effno "Effective Number of Ethnic Groups in Government"
lab var effno_change "Change in Nx between t and t+1"

* Generate country fixed effects
qui egen id=group(country)
qui tab country, gen(Iccode)

* Generate country time trend
local i=1
while (`i'< 15){
	quietly gen Iccyear`i' = Iccode`i'*year
	label variable Iccyear`i' "Country-Specific Time Trend for Iccode`i'"
	local i = `i' + 1
}

* Generate time fixed effects
qui tab year, gen(time)


keep country year month unrepres
rename country countryname
replace countryname="Congo Kinshasa" if countryname=="CongoKinshasa"
egen country =group(countryname)
fillin country year month
bysort country (year month): replace countryname=countryname[_n-1] if countryname==""
drop if countryname==""
bysort country (year month): replace unrepres=unrepres[_n-1] if unrepres==.
bysort year: egen africa_unrepres = mean(unrepres)
sort year month
gen date=ym(year, month)
format date %tm
rename date Year
lab variable unrepres "Pop. Share of Ethnicities Not Represented in Government"
lab variable africa_unrepres "Pop. Share of Ethnicities Not Represented in Government"

***********************************************************************************
*Figure 2
***********************************************************************************
*twoway  (line unrepres Year,  cmiss(n)), by(countryname) ytitle(,size(small)) xlabel(none) note("")
gen y = year
twoway  (line unrepres y,  cmiss(n)), by(countryname) ytitle(,size(small)) xlabel(1960 1980 2004,valuelab) note("")
** Edit with graph editor
graph save excluded.gph,replace
graph export excluded_edit.eps,replace

***********************************************************************************
*Table 1 - Africa
***********************************************************************************
gen Country=""
gen Mean=.
gen StDev=.
gen Obs=.
global n=1
foreach c of global country {
	qui sum unrepres if countryn=="`c'"
	replace Country="`c'" if _n==$n
	replace Mean=r(mean) if _n==$n
	replace StDev=r(sd) if _n==$n
	replace Obs=r(N) if _n==$n
	global n=$n+1
}
list Country Obs Mean StDev if _n<$n

