clear all
set more off

****
* Load data
use "datasets/seats_weights_aer.dta", clear

* Preliminaries: Consider the unselected sample of coalition parties
gen useat_share_all = n_seats/N_SEATS*100
gen upost_share = 0 if useat_share_all~=.
replace upost_share=post_share*100 if post_share~=.
drop if country=="UK"|country=="Spain"
replace country="Germany" if country=="West Germany"

*** Generate elements for disproportionality measures (Gallagher, 1991)
gen lsq    = (upost_share-useat_share_all)^(2)
keep country  govt_name year lsq

*** Country-year collapse 
collapse (sum) lsq, by(country  govt_name year)
replace lsq = (1/2*lsq)^(1/2)
collapse (mean) lsq, by(country year)
rename year Year
rename country Country
lab var Year "Year"
lab var lsq "LSq Disproportionality"

collapse (mean) lsq (sd) lsq_sd=lsq , by(Country)
li ,clean noobs
