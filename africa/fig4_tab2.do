clear all
set more off

use "datasets/tmp1_orig.dta", clear
rename country Country
rename year Year
sort Country Year
twoway  (line lsq Year, xlabel(1960 1980 2004) ylabel(0 30 60)) , by(Country)
rename Country country

gen Country=""
gen Mean=.
gen StDev=.
gen Obs=.
global n=1
foreach c of global country {
	qui sum lsq if country=="`c'"
	replace Country="`c'" if _n==$n
	replace Mean=r(mean) if _n==$n
	replace StDev=r(sd) if _n==$n
	replace Obs=r(N) if _n==$n
	global n=$n+1
}
list Country Obs Mean StDev if _n<$n
