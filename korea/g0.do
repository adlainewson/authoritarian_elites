* Bring in data
do header.do

*********************
** NUMBER OF EVENTS
*********************
preserve
   * Show n events over time, yearly
   contract evid year regime
   gen nevents=1
   collapse (sum) nevents,by(year regime)
   #delim ;
   twoway 
      scatter nevents year , 
         xline(2012) 
         //text(45 2012 "KJU becomes Supreme Leader",
            //place(e) 
            //orientation(vertical) 
            //size(vsmall)
            //)
         xline(2008) 
         msize(2)
         //text(45 2008 "Kim Jong Il Stroke",
         //   place(w) 
         //   orientation(vertical) 
         //   size(vsmall)
         //   )
         //xline(2010.5 ,
         //   lwidth(27) lc(gs15)
         //   )
         //|| 
         //lfit nevents year,
      //title("Events per year",size(small))
      //xtitle("year",size(small))
      ytitle(" ")
      xtitle(" ")
      xmtick(1995(2)2015)
      xlabel(1994(2)2016, angle(45) )
      //labsize(vsmall))
      //ylabel(,labsize(vsmall))
      ymtick(#4)
      //legend(
      //   pos(6) col(2)
      //   label(1 "Number of Events")
      //   // label(2 "Linear Fit")
      //   )
      ;
      #delim cr
      
      * Save out
      local title events
      graph export figures/`title'.pdf,replace
restore

*********************
** NUMBER OF EVENTS P ELITE
*********************
preserve
   * Number of events per elite
   gen nevents =1
   collapse (sum) nevents,by(elid year regime)
   collapse (mean) nevents, by(year regime)
   #delim ;
   twoway 
      scatter nevents year , 
         xline(2012) 
         //text(65 2013 "KJU becomes Supreme Leader",
         //   place(e) 
         //   orientation(vertical) 
         //   size(vsmall)
         //   )
         xline(2008) 
         msize(2)
         //text(45 2008 "Kim Jong Il Stroke",
         //   place(w) 
         //   orientation(vertical) 
         //   size(vsmall)
         //   )
         //xline(2010.5 ,
         //   lwidth(27) lc(gs15)
         //   )
         //|| 
         //lfit nevents year,
      //title("Average Events per Elite",size(small))
      //xtitle("year",size(small))
      ytitle(" ")
      xtitle(" ")
      xmtick(1995(2)2015)
      xlabel(1994(2)2016, angle(45) )
      //labsize(vsmall))
      ylabel(0(5)35)
      //,labsize(vsmall))
      ymtick(2.5(5)35)
      //legend(
      //   pos(6) col(2)
      //   label(1 "Average Number of Events Attended")
      //   label(2 "Linear Fit")
      //   )
      ;
      #delim cr
      * Save out
      local title events_per_elite
      graph export figures/`title'.pdf,replace
restore


preserve
   * Elites per Event
   contract nelites evid year regime
   collapse (mean) nelites,by(year regime)
   #delim ;
   twoway 
      scatter nelites year , 
         xline(2012) 
         xline(2008) 
         //xline(2010.5 ,
         //   lwidth(27) lc(gs15)
         //   )
         //|| 
         //lfit nelites year,
      //title("Elites per Event",size(small))
      //xtitle("year",size(small))
      msize(2)
      xtitle(" ")
      ytitle(" ")
      xmtick(1995(2)2015)
      xlabel(1994(2)2016, angle(45) )
      //labsize(vsmall))
      ylabel(0(5)20)
      //,labsize(vsmall))
      ymtick(2.5(5)20)
      //legend(
      //   pos(6) col(2)
      //   label(1 "Average Number of Elites")
      //   label(2 "Linear Fit")
      //   )
      ;
      #delim cr

      * Save out
      local title elites_per_event
      graph export figures/`title'.pdf,replace
restore

