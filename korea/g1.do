do header.do

* Create graph of distribution of elite importance by sector-type, with importance defined as number of appearances, all by regime
bysort elid regime: gen el_imp = _N
gen imp0 = el_imp
gen logimp0 = log(imp0)
* Elite IDs  for JST, KJU, KJI
replace el_imp = . if elid==70 | elid==155 | elid==160 
local importance_cutoff 30
gen logimp = log(el_imp)
gen logimp2 = log(el_imp) if el_imp > `importance_cutoff'
gen imp = el_imp

contract regime sector elid el_imp logimp* imp

* Top 30 elites:
gen negimp = -imp
bysort regime sector (negimp): gen ic = (_n <= 20)
gen logimp3 = logimp if ic==1

             *title("Importance of Elite Attendees by Sector");
             *subtitle("`subtitle'");
             *note("Importance defined as number of event appearances during the regime." 
             *     "Restricted to elites with more than `importance_cutoff' appearances during the regime."
             *     "Elite counts by event type are `n_m_`cur_reg'', `n_p_`cur_reg'', `n_e_`cur_reg'' respectively"
             *     "Kernel bandwidth `bwl'"
             *     , size(vsmall))
forvalues cur_reg =1/3 {
      if `cur_reg' == 1 {
         local subtitle "Kim Jong Il"
      }
      if `cur_reg' == 2 {
         local subtitle "Transition"
      }
      if `cur_reg' == 3 {
         local subtitle "Kim Jong Un"
      }
      di "`subtitle'"
      count if logimp!=. & sector=="M" & regime==`cur_reg' 
      local n_m_`cur_reg' `r(N)'
      count if logimp!=. & sector=="P" & regime==`cur_reg' 
      local n_p_`cur_reg' `r(N)'
      count if logimp!=. & sector=="E" & regime==`cur_reg' 
      local n_e_`cur_reg' `r(N)'
      local bwl 0.2
      #delim ;
      twoway kdensity logimp2 if sector=="M" & regime==`cur_reg' , bw(`bwl') lwidth(.6) lpattern(dash) ||
             kdensity logimp2 if sector=="P" & regime==`cur_reg', bw(`bwl') lwidth(.6)  lpattern(longdash_dot)||
             kdensity logimp2 if sector=="E" & regime==`cur_reg'
             ,
             lwidth(.6)
             lpattern(dash_dot)
             bw(`bwl')
             ylabel(none)
             xlab(,nogrid)
             xtitle("log(# event appearances)")
             ytitle("kernel density")
             legend(
                pos(6) col(3)
                label(1 "Military")
                label(2 "Party")
                label(3 "Economy")
             )
             ;
       #delim cr
       local export_types "eps pdf"
       *foreach typ of local export_types {
       *   di "graph export figures/`typ'/sector_import_`cur_reg'.`typ',replace"
       *   graph export figures/`typ'/sector_import_`cur_reg'.`typ',replace
       *}
       graph export figures/sector_import_`cur_reg'.pdf,replace
}
