do header.do

* Create graph of distribution of elite importance by sector-type, with importance defined as number of appearances, all by regime
bysort elid regime: gen el_imp = _N
replace el_imp = . if elid==70 | elid==155 | elid==160 
gen logimp = log(el_imp)
gen logimp2 = log(el_imp) if el_imp > 30
gen imp = el_imp

local cur_reg 2
count if logimp2!=. & sector=="M" & regime==`cur_reg' 
count if logimp2!=. & sector=="P" & regime==`cur_reg' 
count if logimp2!=. & sector=="E" & regime==`cur_reg' 

contract regime elid el_imp logimp* imp

* Top 20 elites:
gen negimp = -imp
bysort regime (negimp): gen ic = (_n <= 20)
gen logimp3 = logimp if ic==1

          *title("Inner Circle Importance Distribution")
          *subtitle("by regime")
          *note( "Restricted to elites with more than 30 appearances during the regime."
          *     "Elite counts by regime are `n1', `n2', `n3' respectively"
          *     "Kernel bandwidth `bw'"
          *     , size(vsmall)
          *    )
preserve
   local bw .25
   count if logimp2!=.  & regime== 1
   local n1 `r(N)'
   count if logimp2!=.  & regime== 2
   local n2 `r(N)'
   count if logimp2!=.  & regime== 3
   local n3 `r(N)'
   #delim ;
   twoway kdensity logimp2 if regime==1 ,bw(`bw') lwidth(.6) lpattern(dash) ||
          kdensity logimp2 if regime==2, bw(`bw') lwidth(.6) lpattern(dash_dot) ||
          kdensity logimp2 if regime==3
          ,
          lwidth(.6) lpattern(longdash_dot) bw(`bw')
          xtitle("log(# event appearances)")
          xlab(,nogrid)
          ytitle("kernel density")
          ylabel(none)
          legend(
             pos(6) col(3)
             label(1 "Kim Jong Il")
             label(2 "Transition")
             label(3 "Kim Jong Un")
          )
          ;
   #delim cr
   graph export figures/regimes_import.pdf,replace
restore
