*use ../datasets/nk_data_cleaned.dta,clear
use data.dta,clear

* Summarize event types by regime
*local kji "year>= 2004 & year <= 2007"
*local jst "year <= 2011 & year >= 2008"
*gen year = year(date)
local kji "year>= 2005 & year <= 2008"
local jst "year <= 2011 & year >= 2009"
local kju "year >= 2012 & year <= 2015"
drop if year==2017

gen regime = .
replace regime = 1 if `kji'
replace regime = 2 if `jst'
replace regime = 3 if `kju'
cap label define regimelabel 1 "Kim Jong Il Regime" 2 "Jang Song Thaek Regime" 3 "Kim Jong Un Regime"
cap label values regime regimelabel
